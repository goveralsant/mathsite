---
title: Расписание занятий
type: docs
bookToC: false
description: "Расписание занятий с Климом."
---

<h1 align="center">Расписание занятий</h1>

<p align="center"><img class="uroki" src="/zaplanirovanie_uroki.png" alt="Запланированные уроки"> — Запланированные; <img class="uroki" src="/sostoyavshiesia_uroki.png" alt="Состоявшиеся уроки"> — Состоявшиеся.</p>

<style>
  div#calWrapper {
    overflow-x: auto;
  }

  iframe[src*="calendar.yandex"] {
    width: 100%;
    box-sizing: border-box;
  }

  @media all and (max-width: 500px) {
    iframe[src*="calendar.yandex"] {
    width: 420px;
    box-sizing: border-box;
  }
  }
</style>

<div id="calWrapper">
  <iframe src="https://calendar.yandex.ru/month?embed&amp;layer_ids=10715252,10715639&amp;tz_id=Europe/Moscow" style="border: 3px solid #eee;/*! width: 100%; */" width="1000" height="800" frameborder="0"></iframe>
</div>


